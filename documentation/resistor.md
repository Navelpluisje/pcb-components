# Resistor

An HTML representation of a resistor.

## Default usage

Import the resistor to your project:

```javascript
// ES6
import { resistor } from '@navelpluisje/pcb-components/src';
// Or for example an Electron project
const { resistor } = require('@navelpluisje/pcb-components/dist/umd');

// Then call the function. This will make the component avalaible in the DOM
resistor();
```

When included you can use the `np-resistor` tag to use it in your code.

```
<np-resistor />
```

This will generate the next resistor:

![default dip switch](assets/resistor-1.png)

## Value

By clicking the resistor, or the value, an inputfield will appear. Type the value of the resistor, blur the field and see the color-bands change to the corresponding colors for the value.

## Attributes

* `value`: The initial value for the resistor

> An example with 3 'dips' and the show-value attribute set:
> ```html
> <np-resistor
>   value="470k"
> ></np-resistor>
> ```
>![default dip switch](assets/resistor-value.png)


## Events

The resistor has one event you can listen to: The `change` event. This works like the change event of any input-element. The current value of the resistor is the value property of the event target.

```
  document.querySelector('np-resistor')
    .addEventListener(
      'change',
      (evt) => console.log(evt.target.value)
    );
```
This will print the current value of the resistor in the console
