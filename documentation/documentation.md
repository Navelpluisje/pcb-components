# List of components

Here is a list of the current available components:

* [Dip switch](./dip-switch.md)
* [Trimmer](./trimmer.md)
* [Resistor](./resistor.md)
* [Ic](./ic.md)
* [Led](./led.md)

If you have any idea for more usefull component (needs to be in pcb style) you can [create an issue](https://bitbucket.org/Navelpluisje/pcb-components/issues/new) with a request.
