# Led

An HTML representation of a led.

## Default usage

Import the led to your project:

```javascript
// ES6
import { led } from '@navelpluisje/pcb-components/src';
// Or for example an Electron project
const { led } = require('@navelpluisje/pcb-components/dist/umd');

// Then call the function. This will make the component avalaible in the DOM
led();
```

When included you can use the `np-led` tag to use it in your code.

```html
<np-led />
```

This will generate the next led:

![default dip switch](assets/led-1.png)

## Value

By clicking the led, a color inputfield will appear. Select acolor for the led, blur the field and the change event will be triggered.

## Attributes

* `value`: The initial value for the ic. Can be a color name, hsl, rgb or hax color.

> An example with value set:

```html
 <np-led
   value="rgb(0, 255, 0)"
 ></np-ic>
```

![default dip switch](assets/led-value.png)


## Events

The led has one event you can listen to: The `change` event. This works like the change event of any input-element. The current value of the led is the value property of the event target.

```
  document.querySelector('np-led')
    .addEventListener(
      'change',
      (evt) => console.log(evt.target.value)
    );
```
This will print the current value of the ic in the console
