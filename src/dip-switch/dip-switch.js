

import htmlTemplate from './dip-switch.html';
import style from './dip-switch.css';

export const dipswitch = () => (customElements.define('np-dipswitch', class DipSwitch extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();
    this.currentDocument = document.currentScript.ownerDocument;

    // Get the template and substract the parts
    // const template = document.getElementById('np-dip-switch').import;
    const template = document.createElement('div');
    template.innerHTML = htmlTemplate;
    const templateContent = template.querySelector('.dip-switch');
    this.dipSwitchContent = template.querySelector('.dip-switch-item');

    const styling = document.createElement('style');
    styling.innerHTML = style;

    // Create the shadowRoot and append the content
    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(templateContent.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    // Create Events
    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.value = 0;
    // Get the attributes of our web-component
    this.dips = parseInt(this.getAttribute('dips'), 10) || 1;
    this.color = parseInt(this.getAttribute('color'), 10) || null;
    this.saturation = parseInt(this.getAttribute('saturation'), 10) || null;
    this.showValue = typeof this.getAttribute('show-value') === 'string';

    this.appendDips();
    this.setSwitchValue();
    this.setColor();
  }

  appendDips() {
    const list = this.shadowRoot.querySelector('ul');

    Array(parseInt(this.dips, 10)).fill().forEach((x, index) => {
      list.appendChild(this.createDipSwitchItem(this.dipSwitchContent.cloneNode(true), index));
    });
  }

  createDipSwitchItem(dipSwitch, index) {
    const inp = dipSwitch.querySelector('input');
    const label = dipSwitch.querySelector('label');
    const text = dipSwitch.querySelector('div');
    inp.id = `dip-${index.toString()}`;
    inp.name = inp.id;
    label.setAttribute('for', inp.id);
    text.textContent = (index + 1).toString();

    inp.addEventListener('change', this.setValue.bind(this));
    return dipSwitch;
  }

  setColor() {
    const html = document.querySelector('html');
    const dip = this.shadowRoot.querySelector('.dip-switch');

    if (this.color !== null) {
      dip.style.setProperty('--switch-color', this.color);
    } else if (getComputedStyle(html).getPropertyValue('--switch-color') === '') {
      dip.style.setProperty('--switch-color', '0');
    }

    if (this.saturation !== null) {
      dip.style.setProperty('--switch-saturation', `${this.saturation}%`);
    } else if (getComputedStyle(html).getPropertyValue('--switch-saturation') === '') {
      dip.style.setProperty('--switch-saturation', '50%');
    }
  }

  setValue() {
    const val = [];
    this.shadowRoot.querySelectorAll('input').forEach((inp) => {
      val.push(Number(inp.checked));
    });

    this.value = parseInt(val.join(''), 2);
    this.setSwitchValue();
    this.dispatchEvent(this.changeEvent);
  }

  setSwitchValue() {
    if (this.showValue) {
      const switchValue = this.shadowRoot.querySelector('.dip-switch-value');
      switchValue.textContent = this.value;
    }
  }
}));

export default null;
