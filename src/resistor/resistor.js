import html from './resistor.html';
import style from './resistor.css';

// const RESISTOR_REGEX = /^([0-9]{2}[kM]|[0-9]{1,9})$/;

export const resistor = () => (customElements.define('np-resistor', class Rotary extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();
    this.currentDocument = document.currentScript.ownerDocument;

    // Get the template and substract the parts
    // const template = document.getElementById('np-dip-switch').import;
    const template = document.createElement('div');
    template.innerHTML = html;
    const templateContent = template.querySelector('.resistor');

    const styling = document.createElement('style');
    styling.innerHTML = style;

    // Create the shadowRoot and append the content
    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(templateContent.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    // Create Events
    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.resistor = this.shadowRoot.querySelector('.resistor');
    this.valueField = this.shadowRoot.querySelector('.value');

    // Get the attributes of our web-component
    this.value = this.getAttribute('value') || '0';
    this.min = parseFloat(this.getAttribute('min')) || 0;
    this.max = parseFloat(this.getAttribute('max')) || 10;
    this.step = parseFloat(this.getAttribute('step')) || 0.5;
    this.label = this.getAttribute('label');

    this.color = parseInt(this.getAttribute('color'), 10) || null;
    this.saturation = parseInt(this.getAttribute('saturation'), 10) || null;

    this.valueField.value = this.value;
    this.setEventBindings();
    this.setColorBands();
  }

  setEventBindings() {
    this.resistor.addEventListener('focus', this.handleResistorFocus.bind(this), false);
    this.valueField.addEventListener('focus', this.handleResistorFocus.bind(this), false);
  }

  handleResistorFocus(event) {
    event.target.classList.add('focus');
    this.valueField.contentEditable = 'true';
    this.valueField.focus();
    this.valueField.addEventListener('blur', this.handleValueBlur.bind(this), false);
  }

  handleValueBlur() {
    this.valueField.contentEditable = 'false';
    this.resistor.classList.remove('focus');
    this.valueField.removeEventListener('blur', this.handleValueBlur.bind(this));
    this.setValue(this.valueField.value);
  }

  setColorBands() {
    let value;
    if (this.value.includes('M')) {
      value = parseFloat(this.value) * (10 ** 6);
    } else if (this.value.includes('k')) {
      value = parseFloat(this.value) * (10 ** 3);
    } else {
      value = parseFloat(this.value);
    }
    this.setBandColor(0, value.toString()[0]);
    this.setBandColor(1, value.toString()[1] || '0');

    let power;
    for (let i = -2; i < 10; i += 1) {
      if ((10 ** i) > (value / 100)) {
        power = i;
        break;
      }
    }
    this.setBandColor(2, power);
  }

  setBandColor(index, value) {
    this.shadowRoot.querySelectorAll('.band')[index].classList = [`band color-${value}`];
  }

  setValue(value) {
    if (this.value === value) { return false; }
    this.value = value;
    this.setColorBands();
    this.dispatchEvent(this.changeEvent);
    return true;
  }
}));

export default null;
