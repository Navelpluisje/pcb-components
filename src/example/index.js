import {
  dipswitch,
  trimmer,
  resistor,
  ic,
  led,
} from '../index';

dipswitch();
trimmer();
resistor();
ic();
led();
