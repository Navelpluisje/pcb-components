export const round = (number, step) => {
  const decimals = step.toString().split('.')[1].length;
  const value = Math.round(number / step) * step;
  return parseFloat(value.toFixed(decimals));
};

export default null;
