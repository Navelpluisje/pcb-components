import { Color } from '@navelpluisje/color/src/color';
import htmlTemplate from './led.html';
import style from './led.css';

export const led = () => (customElements.define('np-led', class Led extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();
    this.currentDocument = document.currentScript.ownerDocument;

    // Get the template and substract the parts
    // const template = document.getElementById('np-dip-switch').import;
    const template = document.createElement('div');
    template.innerHTML = htmlTemplate;
    const templateContent = template.querySelector('.led');

    const styling = document.createElement('style');
    styling.innerHTML = style;

    // Create the shadowRoot and append the content
    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(templateContent.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    // Create Events
    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.led = this.shadowRoot.querySelector('.led-wrapper');
    this.colorPicker = this.shadowRoot.querySelector('input');

    this.initColor(this.getAttribute('value') || '#f00');

    this.setEventBindings();
  }

  setEventBindings() {
    this.led.addEventListener('click', this.openColorPicker.bind(this));
    this.colorPicker.addEventListener('input', this.changeColor.bind(this));
  }

  openColorPicker() {
    this.colorPicker.click();
  }

  initColor(value) {
    this.color = new Color(value);
    this.setColor();
  }

  changeColor(event) {
    this.color = new Color(event.target.value);
    this.setColor();
  }

  setColor() {
    const value = this.color.getHsl();
    this.led.style.setProperty('--led-color', value);
    this.setValue(value);
  }

  setValue(value) {
    if (this.value === value) { return false; }
    this.value = value;
    this.dispatchEvent(this.changeEvent);
    return true;
  }
}));

export default null;
