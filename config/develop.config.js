const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); // eslint-disable-line import/no-extraneous-dependencies
const StyleLintPlugin = require('stylelint-webpack-plugin'); // eslint-disable-line import/no-extraneous-dependencies
const webpack = require('webpack'); // eslint-disable-line import/no-extraneous-dependencies
const commonConfig = require('./common.config');

const developConfig = {
  entry: './src/example/index.js',
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, '../example'),
  },
  mode: 'development',
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: './src/example/index.html',
    }),
    new StyleLintPlugin({
      configFile: './config/stylelintrc',
      ignorePath: './config/.lintignore',
      files: './src/**/*.s?(a|c)ss',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'example'),
    port: 9000,
    hot: true,
  },
};

module.exports = Object.assign({}, commonConfig, developConfig);
