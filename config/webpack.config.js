const path = require('path');
const commonConfig = require('./common.config');

const umdConfig = {
  entry: './src/index.js',
  output: {
    filename: 'umd.js',
    path: path.resolve(__dirname, '../dist'),
    library: 'npComponents',
    libraryTarget: 'umd',
  },
  mode: 'production',
  target: 'node',
};

const cjsConfig = {
  entry: './src/index.js',
  output: {
    filename: 'commonjs.js',
    path: path.resolve(__dirname, '../dist'),
    library: 'npComponents',
    libraryTarget: 'commonjs',
  },
  mode: 'production',
};

module.exports = [
  Object.assign({}, commonConfig, umdConfig),
  Object.assign({}, commonConfig, cjsConfig),
];
