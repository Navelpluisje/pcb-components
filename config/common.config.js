module.exports = {
  module: {
    rules: [
      {
        test: /\.html$/,
        use: 'raw-loader',
      },
      {
        test: /\.css$/,
        use: 'css-loader',
      }, {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          configFile: 'config/.eslintrc.json',
          ignorePath: 'config/.lintignore',
        },
      },
    ],
  },
};
