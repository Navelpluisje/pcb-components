# PCB Components

[![npm (scoped)](https://img.shields.io/npm/v/@navelpluisje/pcb-components.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/pcb-components)
[![npm (scoped)](https://img.shields.io/npm/dy/@navelpluisje/pcb-components.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/pcb-components)
[![npm (scoped)](https://img.shields.io/npm/l/@navelpluisje/pcb-components.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/pcb-components)
[![npm (scoped)](https://img.shields.io/bitbucket/issues-raw/navelpluisje/pcb-components.svg?style=flat-square)](https://www.npmjs.com/package/@navelpluisje/pcb-components)
[![npm (scoped)](https://img.shields.io/badge/made%20with-%E2%9D%A4-red.svg?style=flat-square)](https://navelpluisje.nl)

## About

This is a small library with PCB related components. They all act like input fields. All components were writen as native web components and then bundled with webpack.

## Usage

### install
```
  npm i -S @navelpluisje/pcb-components
```

### ES6 import

### Javascript

```javascript
import { resistor } from '@navelpluisje/pcb-components/src';

resistor();
```

### HTML

```html
<np-resistor value="47k"></np-resistor>
```
### Electron application

### Javascript

```javascript
const { resistor } = require('@navelpluisje/pcb-components/dist/umd');

resistor();
```

### HTML

```html
<np-resistor value="47k"></np-resistor>
```

Check the [documentation](./documentation/documentation.md)


## Issues

If you found a bug, room for improvement or whatever, you can [create an issue](https://bitbucket.org/Navelpluisje/pcb-components/issues?status=new&status=open) here.

Please check first if your issue already hes been reported.

## Requests

If you have any idea for more usefull component (needs to be in pcb style) you can [create an issue](https://bitbucket.org/Navelpluisje/pcb-components/issues/new) with a request.
